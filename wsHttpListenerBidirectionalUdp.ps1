param( 
   [string] $listen = 'http://localhost:5001/',  # Prefix
   [string] $remote_address = "127.0.0.1",
   [string] $local_address = "127.0.0.1",
   [int]    $remote_port=50007,
   [int]    $local_port=50008,
   [switch] $multicast = $false
)

write-host "Web Listener: Start"

# maybe need to allow script execution via
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process

try {
   $listener = New-Object System.Net.HttpListener
   $listener.Prefixes.Add($listen)
   $listener.Start()
}
catch {
   write-error "Unable to open listener. Check Admin permission or NETSH Binding"
   exit 1
}

$recv_job = {
   param($ws, $udpclient, $localEp)
   $bufferSize = 65507
   $buffer = [Net.WebSockets.WebSocket]::CreateClientBuffer($bufferSize, $bufferSize)
   $ct = [Threading.CancellationToken]::new($false)
   $taskResult = $null

   while ($ws.State -eq [Net.WebSockets.WebSocketState]::Open) {
      do {
         $taskResult = $ws.ReceiveAsync($buffer, $ct)
         while (-not $taskResult.IsCompleted -and $ws.State -eq [Net.WebSockets.WebSocketState]::Open) {
               [Threading.Thread]::Sleep(10)
         }
         $udpclient.SendAsync($buffer, $taskResult.Result.Count).GetAwaiter().GetResult() | Out-Null
         $msg_send = [System.Text.Encoding]::UTF8.GetString($buffer, 0, $taskResult.Result.Count )
         "Send message: $msg_send" | Out-File -FilePath "logs.txt" -Append
      } until (
         $ws.State -ne [Net.WebSockets.WebSocketState]::Open -or $taskResult.Result.EndOfMessage
      )
   }
}

$send_job = {
   param($ws, $udpclient, $localEp)

   $ct = New-Object Threading.CancellationToken($false)
   while ($ws.State -eq [Net.WebSockets.WebSocketState]::Open){
   try{
      $udpReceiveResult = $udpclient.ReceiveAsync().GetAwaiter().GetResult()
      $buffer = $udpReceiveResult.Buffer
      $bytesReceived = $buffer.Length
      if($bytesReceived -gt 0){
         [ArraySegment[byte]]$msg = [ArraySegment[byte]]::new($buffer, 0, $bytesReceived ) 
         $ws.SendAsync(
               $msg,
               [System.Net.WebSockets.WebSocketMessageType]::Binary, # ::Text
               $true,
               $ct
         ).GetAwaiter().GetResult() | Out-Null
      }
   } catch {
      # "Catched in send_job $Error[0]" | Out-File -FilePath "logs.txt" -Append
      $closetask = $ws.CloseAsync(
         [System.Net.WebSockets.WebSocketCloseStatus]::Empty,
         "",
         $ct
      )
      do { Sleep(1) }
      until ($closetask.IsCompleted)
   }
   }
}

$udpserver = [System.Net.Sockets.UdpClient]::New()
$localEp = [System.Net.IPEndPoint]::New([IPAddress]::Parse($local_address), $local_port);

$udpserver.Client.SetSocketOption([System.Net.Sockets.SocketOptionLevel]::Socket, [System.Net.Sockets.SocketOptionName]::ReuseAddress, $true);
$udpserver.ExclusiveAddressUse = $false;
$udpserver.Client.Bind($localEp);

if($multicast){
   $multicastaddress = [IPAddress]::Parse($remote_address);
   $udpserver.JoinMulticastGroup($multicastaddress);
}

$remoteEp = [System.Net.IPEndPoint]::New([IPAddress]::Parse($remote_address), $remote_port);
$udpclient = [System.Net.Sockets.UdpClient]::New()
$udpclient.ExclusiveAddressUse = $false;
$udpclient.Client.SetSocketOption([System.Net.Sockets.SocketOptionLevel]::Socket, [System.Net.Sockets.SocketOptionName]::ReuseAddress, $true);
$udpclient.Connect($remoteEp);
if($multicast){
   $multicastaddress = [IPAddress]::Parse($local_address);
   $udpclient.JoinMulticastGroup($multicastaddress);
}

# Create a simple Websocke server proving a Message including teh current time
Write-host "Web Listener listening"
[console]::TreatControlCAsInput = $true
while (!([console]::KeyAvailable)) {
   Write-host "Press any key to Stop (requires client to be connected, yet)"
   $context = $listener.GetContextAsync().GetAwaiter().GetResult()
   if ($context.Request.IsWebSocketRequest)
   {
      write-host ("Received Websocket-Request on " + $listener.Prefixes )
      $webSocketContext = $context.AcceptWebSocketAsync(([NullString]::Value)).GetAwaiter().GetResult() # https://docs.microsoft.com/de-de/dotnet/api/system.net.httplistenercontext.acceptwebsocketasync?view=netframework-4.5
      $webSocket = $webSocketContext.WebSocket;
      $cts = New-Object Threading.CancellationTokenSource
      $ct = New-Object Threading.CancellationToken($false)

      try{
         Write-Output "Starting recv runspace"
         $recv_runspace = [PowerShell]::Create()
         $recv_runspace.AddScript($recv_job).
            AddParameter("ws", $webSocket).
            AddParameter("udpclient", $udpclient).BeginInvoke() | Out-Null

         Write-Output "Starting send runspace"
         $send_runspace = [PowerShell]::Create()
         $send_runspace.AddScript($send_job).
            AddParameter("ws", $webSocket).
            AddParameter("udpclient", $udpserver).BeginInvoke() | Out-Null
         do {
            Sleep(1)
         } until ($webSocket.State -ne [Net.WebSockets.WebSocketState]::Open -or ([console]::KeyAvailable))
      }
      finally {
         Write-Host "Closing TCP Stream"
         if($stream) { $stream.close(); }


         Write-Output "Closing WS connection"
         $closetask = $webSocket.CloseAsync(
            [System.Net.WebSockets.WebSocketCloseStatus]::Empty,
            "",
            $ct
         )

         do { Sleep(1) }
         until ($closetask.IsCompleted)
         $webSocket.Dispose()

         Write-Host "Stopping runspaces"
         $recv_runspace.Stop()
         $recv_runspace.Dispose()

         $send_runspace.Stop()
         $send_runspace.Dispose()
      }

   }
   $response = $context.Response
   $response.OutputStream.close()
}
Write-host "Web Listener Stopping"
$listener.Stop()
# $udpclient.Dispose()