param( 
   [string] $listen = 'http://localhost:5001/',  # Prefix
   [string] $tcp_address = "localhost",
   [int]    $port=50007
)

write-host "Web Listener: Start"

# maybe need to allow script execution via
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope Process

try {
   $listener = New-Object System.Net.HttpListener
#    $listener.Prefixes.Add('http://+:81/')  # listen on ony interface (matching netsh or running on admin console required)
   $listener.Prefixes.Add($listen) 
   $listener.Start()
}
catch {
   write-error "Unable to open listener. Check Admin permission or NETSH Binding"
   exit 1
}

$recv_job = {
   param($ws, $stream)

   $buffer = [Net.WebSockets.WebSocket]::CreateClientBuffer(1024,1024)
   $ct = [Threading.CancellationToken]::new($false)
   $taskResult = $null

   while ($ws.State -eq [Net.WebSockets.WebSocketState]::Open) {
      do {
         $taskResult = $ws.ReceiveAsync($buffer, $ct)
         while (-not $taskResult.IsCompleted -and $ws.State -eq [Net.WebSockets.WebSocketState]::Open) {
               [Threading.Thread]::Sleep(10)
         }
         $stream.WriteAsync($buffer, 0, $taskResult.Result.Count, $ct).GetAwaiter().GetResult() | Out-Null
      } until (
         $ws.State -ne [Net.WebSockets.WebSocketState]::Open -or $taskResult.Result.EndOfMessage
      )
   }
}

$send_job = {
   param($ws, $stream)

   $ct = New-Object Threading.CancellationToken($false)
   $workitem = $null
   $buffersize=2048
   $buffer = new-object System.Byte[] $buffersize;

   while ($ws.State -eq [Net.WebSockets.WebSocketState]::Open -and $stream.CanRead){
   try{
      $bytesReceived = $stream.ReadAsync($buffer, 0, $buffersize, $ct).GetAwaiter().GetResult()
      
      # "bytesReceived $bytesReceived" | Out-File -FilePath "logs.txt" -Append
      if($bytesReceived -gt 0){
         # $msg_recvd = [System.Text.Encoding]::UTF8.GetString($buffer, 0, $bytesReceived )
         # "Received TCP Message $msg_recvd" | Out-File -FilePath "logs.txt" -Append

         [ArraySegment[byte]]$msg = [ArraySegment[byte]]::new($buffer, 0, $bytesReceived ) 
         # [ArraySegment[byte]]$msg = [Text.Encoding]::UTF8.GetBytes($msg_recvd)
         $ws.SendAsync(
               $msg,
               [System.Net.WebSockets.WebSocketMessageType]::Binary, # ::Text
               $true,
               $ct
         ).GetAwaiter().GetResult() | Out-Null
         # "Send message: $msg_recvd" | Out-File -FilePath "logs.txt" -Append
      }
   } catch {
      $closetask = $ws.CloseAsync(
         [System.Net.WebSockets.WebSocketCloseStatus]::Empty,
         "",
         $ct
      )
      do { Sleep(1) }
      until ($closetask.IsCompleted)
   }
   }
}

# Create a simple Websocke server proving a Message including teh current time
Write-host "Web Listener listening"
[console]::TreatControlCAsInput = $true
while (!([console]::KeyAvailable)) {
   Write-host "Press any key to Stop (requires client to be connected, yet)"
   $context = $listener.GetContextAsync().GetAwaiter().GetResult()
   if ($context.Request.IsWebSocketRequest)
   {
      write-host ("Received Websocket-Request on " + $listener.Prefixes )
      $webSocketContext = $context.AcceptWebSocketAsync(([NullString]::Value)).GetAwaiter().GetResult() # https://docs.microsoft.com/de-de/dotnet/api/system.net.httplistenercontext.acceptwebsocketasync?view=netframework-4.5
      $webSocket = $webSocketContext.WebSocket;
      $cts = New-Object Threading.CancellationTokenSource
      $ct = New-Object Threading.CancellationToken($false)

      try{
         # connect to remote socket
         $tcpConnection = new-object System.Net.Sockets.TcpClient($tcp_address, $port);
         $stream = $tcpConnection.GetStream();

         Write-Output "Starting recv runspace"
         $recv_runspace = [PowerShell]::Create()
         $recv_runspace.AddScript($recv_job).
            AddParameter("ws", $webSocket).
            AddParameter("stream", $stream).BeginInvoke() | Out-Null

         Write-Output "Starting send runspace"
         $send_runspace = [PowerShell]::Create()
         $send_runspace.AddScript($send_job).
            AddParameter("ws", $webSocket).
            AddParameter("stream", $stream).BeginInvoke() | Out-Null

         do {
            $msg = $null
            Sleep(1)
         } until ($webSocket.State -ne [Net.WebSockets.WebSocketState]::Open -or ([console]::KeyAvailable))
      }
      finally {
         Write-Host "Closing TCP Stream"
         if($stream) { $stream.close(); }


         Write-Output "Closing WS connection"
         $closetask = $webSocket.CloseAsync(
            [System.Net.WebSockets.WebSocketCloseStatus]::Empty,
            "",
            $ct
         )

         do { Sleep(1) }
         until ($closetask.IsCompleted)
         $webSocket.Dispose()

         Write-Host "Stopping runspaces"
         $recv_runspace.Stop()
         $recv_runspace.Dispose()

         $send_runspace.Stop()
         $send_runspace.Dispose()
      }

   }
   $response = $context.Response
   $response.OutputStream.close()
}
Write-host "Web Listener Stopping"
$listener.Stop()